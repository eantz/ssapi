from flask import request
from flask_restful import Resource, Api, reqparse
import sys, os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../common")
from dbconnect import connection
from MySQLdb import escape_string
import gc

class Category(Resource):
    def get(self, category_id):
        try:
            c, conn = connection()
            c.execute("SELECT id, parent_id, name FROM categories WHERE id = (%s)",
                [category_id])

            category = c.fetchone()
            data = {}

            if category is not None:
                child_data = {}
                c.execute("SELECT id, parent_id, name FROM categories WHERE parent_id = (%s)",
                    [category[0]])
                child = c.fetchone()

                while child is not None:
                    child_data.update({child[0] : {'id':child[0], 'name': child[2]}})
                    child = c.fetchone()

                data = {'id':category[0],
                        'parent_id':category[1],
                        'name':category[2],
                        'children': child_data}

            return data
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()


    def put(self, category_id):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('parent_id', type=int, help='Category is required')
        parser.add_argument('name', type=str, required=True, help='Name is required')

        args = parser.parse_args()

        # Check Parent ID
        if args['parent_id'] is not None:
            try:
                c, conn = connection()

                c.execute("SELECT id, parent_id, name FROM categories WHERE id = %s", [args['parent_id']])

                parent = c.fetchone()

                if parent is None:
                    return {'message': 'Parent ID is not found'}, 406
                elif parent[1] is not None:
                    return {'message': 'Parent is already a child of another parent'}, 406

            except Exception as e:
                return {'message': str(e)}, 500
            finally:
                conn.commit()
                c.close()
                conn.close()
                gc.collect()

        # check category name
        try:
            c, conn = connection()

            c.execute("SELECT id, parent_id, name FROM categories WHERE name = %s AND id <> %s", (args['name'], category_id))

            category_exist = c.fetchone()

            if category_exist is not None:
                return {'message': 'Category name is already exist'}, 406

        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

        # Update data
        try:
            c, conn = connection()

            if args['parent_id'] == None:
                c.execute("UPDATE categories SET name = %s WHERE id = %s", 
                    (args['name'], category_id))
            else:
                c.execute("UPDATE categories SET parent_id = %s, name = %s WHERE id = %s", 
                    (args['parent_id'], args['name'], category_id))

            c.execute("SELECT id, parent_id, name FROM categories WHERE id = (%s)",
                [category_id])

            category = c.fetchone()

            data = {}

            if(category is not None):
                child_data = {}
                c.execute("SELECT id, parent_id, name FROM categories WHERE parent_id = (%s)",
                    [category[0]])
                child = c.fetchone()

                while child is not None:
                    child_data.update({child[0] : {'id':child[0], 'name': child[2]}})
                    child = c.fetchone()

                data = {'id':category[0],
                        'parent_id':category[1],
                        'name':category[2],
                        'children': child_data}

            return data
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

    def delete(self, category_id):
        try:
            c, conn = connection()
            c.execute("DELETE FROM categories WHERE id = (%s)",
                [category_id])

            return {'message': 'Category removed'}
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

class Categories(Resource):
    def get(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('parent_id', type=int, help='Parent Category should be integer')
        parser.add_argument('name', type=str, help='Name is required')

        args = parser.parse_args()

        query = "SELECT id, parent_id, name FROM categories "
        where = ""
        queryargs = []

        for key, value in args.iteritems():
            if(value is not None):
                if(where != ""):
                    where += " AND " + key + " = %s"
                else:
                    where += "WHERE " + key + " = %s"

                queryargs.append(value)

        try:
            c, conn = connection()

            c.execute(query + where, queryargs)

            categories = c.fetchall()
            results = {}

            for category in categories:
                child_data = {}
                c.execute("SELECT id, parent_id, name FROM categories WHERE parent_id = (%s)",
                    [category[0]])
                child = c.fetchone()

                while child is not None:
                    child_data.update({child[0] : {'id':child[0], 'name': child[2]}})
                    child = c.fetchone()

                data = {'id':category[0],
                        'parent_id':category[1],
                        'name':category[2],
                        'children': child_data}
        
                results.update({category[0] : data})
                category = c.fetchone()


            return results
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

    def post(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('parent_id', type=int, help='Parent Category should be integer')
        parser.add_argument('name', type=str, required=True, help='Category Name is required')

        args = parser.parse_args()

        # Check Parent ID
        if args['parent_id'] is not None:
            try:
                c, conn = connection()

                c.execute("SELECT id, parent_id, name FROM categories WHERE id = %s", [args['parent_id']])

                parent = c.fetchone()

                if parent is None:
                    return {'message': 'Parent ID is not found'}, 406
                elif parent[1] is not None:
                    return {'message': 'Parent is already a child of another parent'}, 406

            except Exception as e:
                return {'message': str(e)}, 500
            finally:
                conn.commit()
                c.close()
                conn.close()
                gc.collect()

        # check category name
        try:
            c, conn = connection()

            c.execute("SELECT id, parent_id, name FROM categories WHERE name = %s", [args['name']])

            category_exist = c.fetchone()

            if category_exist is not None:
                return {'message': 'Category name is already exist'}, 406

        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

        # insert data
        try:
            c, conn = connection()    

            if(args['parent_id'] == None):
                c.execute("INSERT INTO categories (name) VALUES (%s)",
                    [escape_string(args['name'])])
            else:
                c.execute("INSERT INTO categories (parent_id, name) VALUES (%s, %s)",
                    (args['parent_id'], escape_string(args['name'])))

            c.execute("SELECT id, parent_id, name FROM categories WHERE id = %s", [c.lastrowid])

            category = c.fetchone()

            data = {'id':category[0],
                    'parent_id':category[1],
                    'name':category[2]}

            
            return data
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()