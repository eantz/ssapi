from flask_restful import Resource, Api
import sys, os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../common")
from dbconnect import connection

class HelloWorld(Resource):
    def get(self):
        try:
            c, conn = connection()
            return {'hello': 'world'}
        except Exception as e:
            return(str(e))
