from flask import request
from flask_restful import Resource, Api, reqparse
import sys, os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../common")
from dbconnect import connection
from MySQLdb import escape_string
import gc

class Product(Resource):
    def get(self, product_id):
        try:
            c, conn = connection()
            c.execute("SELECT id, category_id, sku, name, color, size, price FROM products WHERE id = (%s)",
                [product_id])

            product = c.fetchone()
            data = {}

            if(product is not None):
                data = {'id':product[0],
                        'category_id':product[1],
                        'sku':product[2],
                        'name':product[3],
                        'color':product[4],
                        'size':product[5],
                        'price':str(product[6])}

            return data
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

    def put(self, product_id):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('category_id', type=int, required=True, help='Category is required')
        parser.add_argument('sku', type=str, required=True, help='SKU is required')
        parser.add_argument('name', type=str, required=True, help='Name is required')
        parser.add_argument('color', type=str, required=True, help='Color is required')
        parser.add_argument('size', type=str, required=True, help='Size is required')
        parser.add_argument('price', type=float, required=True, help='Size is required')

        args = parser.parse_args()

        # Check Category ID
        try:
            c, conn = connection()

            c.execute("SELECT id, parent_id, name FROM categories WHERE id = %s",
                [args['category_id']])

            if c.fetchone() is None:
                return {'message': 'Category ID not found'}, 406

        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

        # Check SKU
        try:
            c, conn = connection()

            c.execute("SELECT id, category_id, sku, name, color, size, price FROM products WHERE sku = %s AND id <> %s",
                (args['sku'], product_id))

            if c.fetchone() is not None:
                return {'message': 'SKU is already used'}, 406

        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()


        try:
            c, conn = connection()

            c.execute("UPDATE products SET category_id = %s, sku = %s, name = %s, color = %s, size = %s, price = %s WHERE id = %s", 
                (args['category_id'], escape_string(args['sku']), escape_string(args['name']), escape_string(args['color']), 
                escape_string(args['size']), args['price'], product_id))

            c.execute("SELECT id, category_id, sku, name, color, size, price FROM products WHERE id = (%s)",
                [product_id])

            product = c.fetchone()
            data = {}

            if(product is not None):
                data = {'id':product[0],
                        'category_id':product[1],
                        'sku':product[2],
                        'name':product[3],
                        'color':product[4],
                        'size':product[5],
                        'price':str(product[6])}

            return data
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

    def delete(self, product_id):
        try:
            c, conn = connection()
            c.execute("DELETE FROM products WHERE id = (%s)",
                [product_id])

            return {'message': 'Product removed'}
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

class Products(Resource):
    def get(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('category_id', type=int)
        parser.add_argument('sku', type=str)
        parser.add_argument('name', type=str)
        parser.add_argument('color', type=str)
        parser.add_argument('size', type=str)
        parser.add_argument('price_min', type=float)
        parser.add_argument('price_max', type=float)

        args = parser.parse_args()

        try:
            c, conn = connection()

            query = "SELECT id, category_id, sku, name, color, size, price FROM products "
            where = ""
            queryargs = []

            for key, value in args.iteritems():
                if(value is not None):
                    if key == 'price_min':
                        if(where != ""):
                            where += " AND price >= %s"
                        else:
                            where += "WHERE price >= %s"
                    elif key == 'price_max':
                        if(where != ""):
                            where += " AND price <= %s"
                        else:
                            where += "WHERE price <= %s"
                    else:
                        if(where != ""):
                            where += " AND " + key + " = %s"
                        else:
                            where += "WHERE " + key + " = %s"

                    queryargs.append(value)

            c.execute(query + where, queryargs)

            product = c.fetchone()
            results = {}

            while product is not None:
                data = {'id':product[0],
                        'category_id':product[1],
                        'sku':product[2],
                        'name':product[3],
                        'color':product[4],
                        'size':product[5],
                        'price':str(product[6])}
		
                results.update({product[0] : data})
                product = c.fetchone()
				
            return results	
            # return {'message':'hello'}
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

    def post(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('category_id', type=int, required=True, help='Category is required')
        parser.add_argument('sku', type=str, required=True, help='SKU is required')
        parser.add_argument('name', type=str, required=True, help='Name is required')
        parser.add_argument('color', type=str, required=True, help='Color is required')
        parser.add_argument('size', type=str, required=True, help='Size is required')
        parser.add_argument('price', type=float, required=True, help='Size is required')

        args = parser.parse_args()

        # Check Category ID
        try:
            c, conn = connection()

            c.execute("SELECT id, parent_id, name FROM categories WHERE id = %s",
                [args['category_id']])

            if c.fetchone() is None:
                return {'message': 'Category ID not found'}, 406

        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()

        # Check SKU
        try:
            c, conn = connection()

            c.execute("SELECT id, category_id, sku, name, color, size, price FROM products WHERE sku = %s",
                [args['sku']])

            if c.fetchone() is not None:
                return {'message': 'SKU is already used'}, 406

        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()



        try:
            c, conn = connection()

            c.execute("INSERT INTO products (category_id, sku, name, color, size, price) VALUES (%s, %s, %s, %s, %s, %s)",
                (args['category_id'], escape_string(args['sku']), escape_string(args['name']), 
                escape_string(args['color']), escape_string(args['size']), args['price']))

            c.execute("SELECT id, category_id, sku, name, color, size, price FROM products WHERE id = (%s)",
                [c.lastrowid])

            product = c.fetchone()
            data = {}

            if(product is not None):
                data = {'id':product[0],
                        'category_id':product[1],
                        'sku':product[2],
                        'name':product[3],
                        'color':product[4],
                        'size':product[5],
                        'price':str(product[6])}

            return data

            # return {'message': 'Product added'}
        except Exception as e:
            return {'message': str(e)}, 500
        finally:
            conn.commit()
            c.close()
            conn.close()
            gc.collect()
