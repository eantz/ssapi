from flask import Flask, request
from flask_restful import Resource, Api
from resources.hello import HelloWorld
from resources.categories import Category, Categories
from resources.products import Product, Products
import gc

app = Flask(__name__)
api = Api(app)

# routing
api.add_resource(HelloWorld, '/')
api.add_resource(Category, '/category/<int:category_id>')
api.add_resource(Categories, '/category')
api.add_resource(Product, '/product/<int:product_id>')
api.add_resource(Products, '/product')
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)