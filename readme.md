Sample API server of product management.

### Dependencies :
* flask
* flask-restful
* python-MySQLdb / MySQL-python

# How To

## Configuration
+ read [standard flask installation](http://flask.pocoo.org/docs/0.10/installation/)
+ make a database with structure in db_structure.sql
+ modify common/dbconnect to match your database detail
+ run app.py

## Categories
| Method   | URL  | Parameter      | Output(json)
|------|----------------|---------|--------------
| GET  | http://_apiurl_/category/_categoryid_   |        | single object
| GET  | http://_apiurl_/category   | parent_id, name (all optional) | multiple and nested object
| POST | http://_apiurl_/category   | parent_id (optional), name     | single object (recently created record)
| PUT  | http://_apiurl_/category/_categoryid_  | parent_id (optional), name | single object (updated record)
| DELETE | http://_apiurl_/category/_categoryid_ |      | message

## Product
| Method   | URL  | Parameter      | Output (json)
|------|----------------|---------|--------------
| GET  | http://_apiurl_/product/_productid_   |        | single object
| GET  | http://_apiurl_/product   | category_id, name, sku, size, color, price\_min, price\_max (all optional) | multiple object
| POST | http://_apiurl_/product   | category_id, name, sku, size, color, price (all required)     | single object (recently created record)
| PUT  | http://_apiurl_/product/_productid_  | category_id, name, sku, size, color, price (all required) | single object (updated record)
| DELETE | http://_apiurl_/product/_productid_ |      | message